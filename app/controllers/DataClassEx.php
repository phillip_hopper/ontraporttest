<?php


namespace Controllers;

use DataClass;

if (!defined('AUTHORIZED')) die();

class DataClassEx extends DataClass
{
    public function SaveNow($db)
    {
        if ($this->HasChanged()) {
            parent::Save($db, $this->table_name, 'id');
            return true;
        }

        return false;
    }
}
