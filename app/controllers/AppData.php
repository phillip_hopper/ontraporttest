<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 10/7/16
 * Time: 4:09 PM
 */

namespace Controllers;

use Database;
use DataSettings;
use Exception;
use Sibertec\Helpers\DateHelper;
use Sibertec\Helpers\StringHelper;


if (!defined('AUTHORIZED')) die();

class AppData
{
    /** @var Database */
    private static $main_database;

    /**
     * @return Database
     * @throws Exception
     */
    public static function MainDatabase()
    {
        if (empty(self::$main_database)) {
            $settings_file = StringHelper::RealPathCombine(CONFIG_DIR, 'data_settings.json');

            if (!is_file($settings_file)) {
                throw new Exception(sprintf('File "%s" could not be found.', $settings_file));
            }

            self::$main_database = Database::Get_Database(new DataSettings($settings_file));
            self::$main_database->cn->set_charset('utf8');
            self::$main_database->DisableCache();
            self::$main_database->run_sql("SET time_zone = '" . DateHelper::GetTimezoneOffset() . "';");
            self::$main_database->run_sql('SET SESSION wait_timeout=3600');
        }

        return self::$main_database;
    }
}
