<?php

declare(strict_types=1);

namespace Helpers;

use Controllers\AppData;
use Exception;
use Sibertec\Helpers\StringHelper;

if (!defined('AUTHORIZED')) die();

class Blacklist
{
    /**
     * @param $email
     *
     * @return bool
     * @throws Exception
     */
    public static function EmailIsBlacklisted($email)
    {
        if (empty($email))
            return false;

        $sql = <<<SQL
SELECT COUNT(*) AS count_of
FROM quotes_2019.blacklist_email
WHERE '{$email}' LIKE CONCAT('%', blacklisted)
SQL;
        return AppData::MainDatabase()->execute_scalar_bool($sql);
    }

    /**
     * @param $sms
     *
     * @return bool
     * @throws Exception
     */
    public static function SmsIsBlacklisted($sms)
    {
        $sms = StringHelper::FormatPhoneSMS($sms, false);

        if (empty($sms))
            return false;

        $sql = <<<SQL
SELECT COUNT(*) AS count_of
FROM quotes_2019.blacklist_sms
WHERE '{$sms}' LIKE CONCAT('%', blacklisted)
SQL;
        return AppData::MainDatabase()->execute_scalar_bool($sql);
    }
}
