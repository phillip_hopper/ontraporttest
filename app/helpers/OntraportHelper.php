<?php /** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Helpers;

use Exception;
use Interfaces\IOntraportSettings;
use Models\OntraportContactFields;
use Models\OntraportContact;
use Models\OntraportDesignUnit;
use Models\OntraportDesignUnitFields;
use Models\OntraportQuote;
use Models\OntraportQuoteFields;
use OntraportAPI\CurlClient;
use OntraportAPI\ObjectType;
use OntraportAPI\Ontraport;
use Sibertec\Helpers\MiscFunctions;
use Sibertec\Helpers\StringHelper;


if (!defined('AUTHORIZED'))
    die();

class OntraportHelper
{
    /** @var Ontraport  */
    private static $client;

    private static $owner_id = 1;
    private static $inventory_lead_tag_id = 2;
    private static $design_lead_tag_id = 3;
    private static $ohio_lead_tag_id = 44;

    private static $pause_milliseconds = 1000;

    public static $source_tags = [
        'camperfactoryoutlet' => 'Camper Factory Outlet Lead',
        'camperwholesalers' => 'Camper Wholesalers Lead',
        'forestriverdirect' => 'Forest River Direct Lead',
        'greatlakesrvcenter' => ' Great Lakes RV Center Lead',
        'heartlandrvsdirect' => 'Heartland RVs Direct Lead',
        'keystonervsdirect' => 'Keystone RVs Direct Lead',
        'midwestrvwholesale' => 'Midwest RV Wholesale Lead',
        'ohiorvnation' => 'Ohio RV Nation Lead',
        'ohiorvwholesale' => 'Ohio RV Wholesale Lead',
        'ohiorvwholesalesuperstore' => 'Ohio RV Wholesale Superstore Lead',
        'palominorvsdirect' => 'Palomino RVs Direct Lead',
        'rockwoodcamper' => 'Rockwood Camper Lead',
        'rv-country' => 'RV Country Lead',
        'rvcapitalwholesalers' => 'RV Capital Wholesalers Lead',
        'rvdiscounters' => 'RV Discounters Lead',
        'rvwholesaleoutlet' => 'RV Wholesale Outlet Lead',
        'rvwholesalers' => 'RV Wholesalers Lead'
    ];

    /**
     * Gets a contact from OntraportHelper, or adds a new one if it does not exist
     *
     * @param string $full_name
     * @param string $source
     * @param string $state
     * @param string $phone
     * @param string $email
     * @param bool $send_sms
     * @param bool $tag_stock
     * @param bool $tag_build
     * @param string $utm_source
     * @param string $utm_medium
     * @param string $utm_campaign
     * @param string $utm_term
     * @param string $utm_content
     *
     * @return OntraportContact
     * @throws Exception
     */
    public static function GetOrCreateContact($full_name, $source, $state, $phone, $email, $send_sms, $tag_stock, $tag_build,
        $utm_source, $utm_medium, $utm_campaign, $utm_term, $utm_content)
    {
        $contact = self::GetContact($phone, $email);

        if (empty($contact)) {
            $contact = self::addContact($full_name, $state, $phone, $email, $send_sms);

            if (empty($contact)) {
                return null;
            }

            $clean_source = preg_replace('/(\.com|\.net)$/', '', strtolower($source));

            if (isset(self::$source_tags[$clean_source])) {
                OntraportHelper::AddTagByName($contact->ID, self::$source_tags[$clean_source]);
            }
            elseif (!empty($clean_source) && !StringHelper::Contains($clean_source, 'localhost') && !StringHelper::Contains($clean_source, 'test')) {
                OntraportHelper::AddTagByName($contact->ID, $clean_source);
            }

            if ((strtoupper($state) == 'OH') || (strtoupper($state) == 'OHIO')) {
                self::AddTag($contact->ID, self::$ohio_lead_tag_id);
            }
        }

        if ($tag_stock) {
            self::AddTag($contact->ID, self::$inventory_lead_tag_id);
        }

        if ($tag_build) {
            self::AddTag($contact->ID, self::$design_lead_tag_id);
        }

        self::UpdateContactUtm($contact, $utm_source, $utm_medium, $utm_campaign, $utm_term, $utm_content);

        return $contact;
    }

    /**
     * @param OntraportContact $contact
     * @param string $phone
     * @param string $email
     * @param bool $send_sms
     * @throws Exception
     */
    public static function UpdateContact($contact, $phone, $email, $send_sms)
    {
        $has_phone = $contact->HasPhone($phone);
        $has_email = $contact->HasEmail($email);

        // if there is nothing to update, return now
        if ($has_phone && $has_email)
            return;

        $phone_field = '';
        $phone_value = '';
        if (!$has_phone) {
            if (($send_sms) && empty($contact->SmsPhone)) {
                $phone_field = OntraportContactFields::$SmsPhone;
                $phone_value = StringHelper::FormatPhoneSMS($phone);
            }
            elseif (empty($contact->OfficePhone)) {
                $phone_field = OntraportContactFields::$OfficePhone;
                $phone_value = StringHelper::FormatPhone($phone);
            }
        }

        $email_field = '';
        if (!$has_email) {
            if (empty($contact->Email))
                $email_field = OntraportContactFields::$Email;
            elseif (empty($contact->Email2))
                $email_field = OntraportContactFields::$Email2;
        }

        // if no fields available to add new data, return now
        if (empty($phone_field) && empty($email_field))
            return;

        $values = array();

        if (!empty($phone_field))
            $values[$phone_field] = $phone_value;

        if (!empty($email_field))
            $values[$email_field] = $email;

        $values['id'] = $contact->ID;

        self::UpdateObject(ObjectType::CONTACT, $values);
    }

    /**
     * Tries to get a contact from Ontraport
     * @param $phone
     * @param $email
     * @return OntraportContact|null
     * @throws Exception
     */
    public static function GetContact($phone, $email)
    {
        MiscFunctions::DebugPrint("Querying Ontraport for Contact {$phone} {$email}.");

        $contact_data = null;

        // first try to look up by email
        if (!empty($email)) {

            // check for apostrophe in the email address
            $email = str_replace("'","\'", $email);

            $condition1 = new OntraportCondition('email', '=', $email);
            $condition2 = new OntraportCondition('f1656', '=', $email);
            $conditions = [
                $condition1,
                'OR',
                $condition2
            ];

            $contact_data = self::GetObject(ObjectType::CONTACT, json_encode($conditions), false);
        }

        if (!empty($contact_data)) {
            MiscFunctions::DebugPrint("Found duplicate for Contact {$email}.");
            return new OntraportContact($contact_data);
        }


        if (empty($phone))
            return null;

        $phone = StringHelper::StripPhone($phone);
        if (strlen($phone) != 10)
            return null;

        $condition1 = new OntraportCondition('sms_number', '=', StringHelper::FormatPhoneSMS($phone));
        $condition2 = new OntraportCondition('office_phone', '=', StringHelper::FormatPhone($phone));
        $conditions = [
            $condition1,
            'OR',
            $condition2
        ];

        $contact_data = self::GetObject(ObjectType::CONTACT, json_encode($conditions), false);

        if (empty($contact_data))
            return null;

        MiscFunctions::DebugPrint("Found duplicate for Contact {$phone}.");
        return new OntraportContact($contact_data);
    }

    /**
     * Adds a new contact to Ontraport
     *
     * @param string $full_name
     * @param string $state
     * @param string $phone
     * @param string $email
     * @param bool $send_sms
     *
     * @return OntraportContact
     * @throws Exception
     */
    private static function addContact($full_name, $state, $phone, $email, $send_sms)
    {
        MiscFunctions::DebugPrint("Adding new Contact {$full_name}.");

        $names = self::SplitFullName($full_name);
        $first_name = $names[0];
        $last_name = $names[1];

        $values = array();

        $values['owner'] = self::$owner_id;

        $values[OntraportContactFields::$FirstName] = $first_name;

        if (!empty($last_name)) {
            $values[OntraportContactFields::$LastName] = $last_name;
        }

        if (!empty($state)) {
            $values[OntraportContactFields::$State] = $state;
        }

        if (!empty($email)) {

            // check for apostrophe in the email address
            // TODO: remove this when Ontraport fixes their API
            $email = str_replace("'","''", $email);

            $values[OntraportContactFields::$Email] = $email;
        }

        if (!empty($phone)) {

            if ($send_sms)
                $values[OntraportContactFields::$SmsPhone] = StringHelper::FormatPhoneSMS($phone);
            else
                $values[OntraportContactFields::$OfficePhone] = StringHelper::FormatPhone($phone);
        }

        $data = self::AddObject(ObjectType::CONTACT ,$values);

        if (empty($data))
            return null;

        return new OntraportContact($data);
    }

    public static function SplitFullName($full_name)
    {
        $return_val = array();
        $names = explode(' ', $full_name);
        if (count($names) > 1) {
            $return_val[] = array_shift($names);
            $return_val[] = implode(' ', $names);
        } else {
            $return_val[] = $full_name;
            $return_val[] = null;
        }
        return $return_val;
    }

    private static function GetClient()
    {
        if (empty(self::$client)) {

            /** @var IOntraportSettings $api_settings */
            $api_settings = json_decode(file_get_contents(StringHelper::RealPathCombine(CONFIG_DIR, 'ontraport_api_settings.json')), false);

            if (!empty($api_settings->debug)) {
                $curl_client = new CurlClient($api_settings->api_key, $api_settings->api_app_id, true, true);
                $curl_client->setLogFile(StringHelper::PathCombine(LOG_DIR, 'curl.log'));
                self::$client = new Ontraport($api_settings->api_app_id, $api_settings->api_key, $curl_client);
            }
            else {
                self::$client = new Ontraport($api_settings->api_app_id, $api_settings->api_key);
            }

            if (!empty($api_settings->pause_milliseconds))
                self::$pause_milliseconds = $api_settings->pause_milliseconds;
        }

        return self::$client;
    }

    /**
     * @param int $object_type_id
     * @param string|string[] $conditions
     * @param bool $and
     *
     * @return array|null
     * @throws Exception
     */
    private static function GetObject($object_type_id, $conditions, $and=true)
    {
        $client = self::GetClient();

        $params = ['objectID' => $object_type_id];

        if (!empty($conditions)) {
            if (is_array($conditions)) {

                if ($and)
                    $conditions = implode(' and ', $conditions);
                else
                    $conditions = implode(' or ', $conditions);
            }

            $params['condition'] = $conditions;
        }

        $content = $client->object()->retrieveMultiple($params);
        $status = $client->getLastStatusCode();

        if ($status >= 400)
            throw new Exception('HTTP Status Code: ' . $status);

        if (strtolower($content) == 'invalid query')
            throw new Exception('API query error: invalid query');

        $response = json_decode($content, true);

        // if the response isn't json, it is an error message
        if (empty($response))
            throw new Exception($content);

        self::WaitForOntraportToCatchUp(self::$pause_milliseconds);

        if (empty($response['data']))
            return null;

        return $response['data'][0];
    }

    /**
     * @param $object_type_id
     * @param $values
     *
     * @return array|string|null
     * @throws Exception
     */
    private static function AddObject($object_type_id, $values)
    {
        $client = self::GetClient();

        $values['objectID'] = $object_type_id;

        $content = $client->object()->create($values);

        return self::GetResponse($client, $content);
    }

    /**
     * @param $object_type_id
     * @param $values
     *
     * @return array|string|null
     * @throws Exception
     */
    private static function UpdateObject($object_type_id, $values)
    {
        $client = self::GetClient();

        $values['objectID'] = $object_type_id;

        $content = $client->object()->update($values);

        return self::GetResponse($client, $content);
    }

    /**
     * @param Ontraport $client
     * @param $content
     *
     * @return array|string|null
     * @throws Exception
     */
    private static function GetResponse(Ontraport $client, string $content)
    {
        $status  = $client->getLastStatusCode();

        if ($status > 399)
            throw new Exception('HTTP Status Code: ' . $status . ' => ' . $content);

        if (strtolower($content) == 'invalid query')
            throw new Exception('API query error: invalid query');

        $response = json_decode($content, true);

        // if the response isn't json, it is an error message
        if (empty($response))
            throw new Exception($content);

        self::WaitForOntraportToCatchUp(self::$pause_milliseconds);

        if (empty($response['data']))
            return null;

        return $response['data'];
    }

    /**
     * @param $contact_id
     * @param $tag_id
     *
     * @return null
     * @throws Exception
     */
    private static function AddTag($contact_id, $tag_id)
    {
        $values = [
            'objectID' => ObjectType::CONTACT,
            'ids'      => $contact_id,
            'add_list' => $tag_id
        ];

        $client = self::GetClient();

        $content = $client->object()->addTag($values);

        return self::GetResponse($client, $content);
    }

    /**
     * @param OntraportContact $contact
     * @param $utm_source
     * @param $utm_medium
     * @param $utm_campaign
     * @param $utm_term
     * @param $utm_content
     *
     * @throws Exception
     */
    public static function UpdateContactUtm($contact, $utm_source, $utm_medium, $utm_campaign, $utm_term, $utm_content)
    {
        $values = array();

        // there is a separate field for referrer now
        if ($utm_source == 'referrer') {
            if (empty($contact->ReferringPage)) {
                $values[OntraportContactFields::$ReferringPage] = $utm_content;
            }
        }
        else {
            $source_id = '0';
            if (!empty($utm_source)) {
                $utm_source = str_replace('+', ' ', $utm_source);
                $source = self::GetOrCreateSource($utm_source);
                if (!empty($source[OntraportContactFields::$FirstUtmSource])) {
                    $source_id = $source[OntraportContactFields::$FirstUtmSource];
                }
            }

            $medium_id = '0';
            if (!empty($utm_medium)) {
                $utm_medium = str_replace('+', ' ', $utm_medium);
                $medium = self::GetOrCreateMedium($utm_medium);
                if (!empty($medium[OntraportContactFields::$FirstUtmMedium])) {
                    $medium_id = $medium[OntraportContactFields::$FirstUtmMedium];
                }
            }

            $campaign_id = '0';
            if (!empty($utm_campaign)) {
                $utm_campaign = str_replace('+', ' ', $utm_campaign);
                $campaign = self::GetOrCreateCampaign($utm_campaign);
                if (!empty($campaign[OntraportContactFields::$FirstUtmCampaign])) {
                    $campaign_id = $campaign[OntraportContactFields::$FirstUtmCampaign];
                }
            }

            $term_id = '0';
            if (!empty($utm_term)) {
                $utm_term = str_replace('+', ' ', $utm_term);
                $term = self::GetOrCreateTerm($utm_term);
                if (!empty($term[OntraportContactFields::$FirstUtmTerm])) {
                    $term_id = $term[OntraportContactFields::$FirstUtmTerm];
                }
            }

            $content_id = '0';
            if (!empty($utm_content)) {
                $utm_content = str_replace('+', ' ', $utm_content);
                $content = self::GetOrCreateContent($utm_content);
                if (!empty($content[OntraportContactFields::$FirstUtmContent])) {
                    $content_id = $content[OntraportContactFields::$FirstUtmContent];
                }
            }

            if (($source_id == '0') && ($campaign_id == '0'))
                return;

            if ($contact->HasFirstUTM()) {
                $values[OntraportContactFields::$LastUtmSource]   = $source_id;
                $values[OntraportContactFields::$LastUtmMedium]   = $medium_id;
                $values[OntraportContactFields::$LastUtmCampaign] = $campaign_id;
                $values[OntraportContactFields::$LastUtmTerm]     = $term_id;
                $values[OntraportContactFields::$LastUtmContent]  = $content_id;
            }
            else {
                $values[OntraportContactFields::$FirstUtmSource]   = $source_id;
                $values[OntraportContactFields::$FirstUtmMedium]   = $medium_id;
                $values[OntraportContactFields::$FirstUtmCampaign] = $campaign_id;
                $values[OntraportContactFields::$FirstUtmTerm]     = $term_id;
                $values[OntraportContactFields::$FirstUtmContent]  = $content_id;
            }
        }

        if (empty($values))
            return;

        $values['id'] = $contact->ID;

        self::UpdateObject(ObjectType::CONTACT, $values);
    }

    /**
     * @param $item_type_id
     * @param $item_name
     *
     * @return array|null
     * @throws Exception
     */
    private static function getOrCreateUtmItem($item_type_id, $item_name)
    {
        $data = self::GetObject(
            $item_type_id,
            '[{"field":{"field":"name"},"op":"=","value":{"value":"' . $item_name . '"}}]',
            false);

        if (!empty($data)) {
            return $data;
        }

        // not found, add it now
        return self::AddObject($item_type_id, ['name' => $item_name]);
    }

    /**
     * @param $source_name
     *
     * @return array|null
     * @throws Exception
     */
    public static function GetOrCreateSource($source_name)
    {
        return self::getOrCreateUtmItem(OntraportContactFields::$UtmSourceTypeID, $source_name);
    }

    /**
     * @param $medium_name
     *
     * @return array|null
     * @throws Exception
     */
    public static function GetOrCreateMedium($medium_name)
    {
        return self::getOrCreateUtmItem(OntraportContactFields::$UtmMediumTypeID, $medium_name);
    }

    /**
     * @param $campaign_name
     *
     * @return array|null
     * @throws Exception
     */
    public static function GetOrCreateCampaign($campaign_name)
    {
        return self::getOrCreateUtmItem(OntraportContactFields::$UtmCampaignTypeID, $campaign_name);
    }

    /**
     * @param $term_name
     *
     * @return array|null
     * @throws Exception
     */
    public static function GetOrCreateTerm($term_name)
    {
        return self::getOrCreateUtmItem(OntraportContactFields::$UtmTermTypeID, $term_name);
    }

    /**
     * @param $content_name
     *
     * @return array|null
     * @throws Exception
     */
    public static function GetOrCreateContent($content_name)
    {
        return self::getOrCreateUtmItem(OntraportContactFields::$UtmContentTypeID, $content_name);
    }


    /**
     * @param string $make_model
     * @param string $category
     * @param string $color
     * @param int $price
     * @param string $option_descriptions
     *
     * @return OntraportDesignUnit
     * @throws Exception
     */
    public static function AddDesignUnit($make_model, $category, $color, $price, $option_descriptions)
    {
        MiscFunctions::DebugPrint("Adding new design unit.");

        $values = [
            'owner'                              => '1',
            OntraportDesignUnitFields::$Model    => $make_model,
            OntraportDesignUnitFields::$Category => $category,
            OntraportDesignUnitFields::$Color    => $color,
            OntraportDesignUnitFields::$Price    => $price,
            OntraportDesignUnitFields::$Options  => $option_descriptions
        ];

        $data = self::AddObject(OntraportDesignUnitFields::$ObjectTypeID, $values);

        unset($values);

        if (empty($data))
            return null;

        return new OntraportDesignUnit($data);
    }

    /**
     * @param $contact_id
     * @param $design_unit_id
     * @param $price
     * @param $discount
     * @param $source
     * @param $option_descriptions
     * @param $owner_id
     * @param $phone
     * @param $email
     * @param $send_sms
     * @param $offer
     *
     * @return OntraportQuote
     * @throws Exception
     */
    public static function AddDesignQuote($contact_id, $design_unit_id, $price, $discount, $source, $option_descriptions, $owner_id, $phone, $email, $send_sms, $offer)
    {
        MiscFunctions::DebugPrint("Adding new design Quote.");

        $values = [
            'owner'                              => $owner_id,
            OntraportQuoteFields::$Source        => $source,
            OntraportQuoteFields::$ContactID     => $contact_id,
            OntraportQuoteFields::$UnitID  => $design_unit_id,
            OntraportQuoteFields::$DesignOptions => $option_descriptions,
            OntraportQuoteFields::$Price         => $price,
            OntraportQuoteFields::$Discount      => $discount,
            OntraportQuoteFields::$Email         => $email,
            OntraportQuoteFields::$Phone         => $phone,
            OntraportQuoteFields::$SendSms       => ($send_sms ? 1 : 0)
        ];

        if (!empty($offer))
            $values[OntraportQuoteFields::$Offer] = $offer;

        $data = self::AddObject(OntraportQuoteFields::$ObjectTypeID, $values);

        unset($values);

        if (empty($data))
            return null;

        return new OntraportQuote($data);
    }

    public static function WaitForOntraportToCatchUp($milliseconds)
    {
        if (empty($milliseconds))
            return;

        usleep($milliseconds * 1000);
    }

    /**
     * @param string|string[] $contact_id
     * @param string|string[] $tag_name
     *
     * @throws Exception
     */
    public static function AddTagByName($contact_id, $tag_name)
    {
        MiscFunctions::DebugPrint('Adding tag by name.');

        $params = [
            'objectID' => ObjectType::CONTACT
        ];

        if (is_array($tag_name))
            $params['add_names'] = $tag_name;
        else
            $params['add_names'] = [$tag_name];

        if (is_array($contact_id))
            $params['ids'] = $contact_id;
        else
            $params['ids'] = [$contact_id];

        $client = self::GetClient();

        $client->object()->addTagByName($params);

        unset($params);
    }

    /**
     * Gets the max Contact ID
     * @return int
     * @throws Exception
     */
    public static function GetMaxContactId()
    {
        MiscFunctions::DebugPrint('Querying Ontraport for Max Contact ID.');

        $return_val = 0;

        $params = [
            'objectID' => ObjectType::CONTACT,
            'range' => 1,
            'sort' => 'id',
            'sortDir' => 'desc',
            'listFields' => 'id'
        ];

        $client = self::GetClient();

        $content = $client->object()->retrieveMultiple($params);

        $data = self::GetResponse($client, $content);

        if (!empty($data))
            $return_val = (int)($data[0]['id'] ?? 0);

        MiscFunctions::DebugPrint("Found Max Contact ID = {$return_val}.");

        return $return_val;
    }
}
