<?php

declare(strict_types=1);

namespace Helpers;


use Database;

if (!defined('AUTHORIZED')) die();

class DataHelper
{
    /**
     *
     * @param Database $db
     * @param string|null $sqlFileName
     * @param string[] $params
     * @param bool $do_not_escape
     *
     * @return array
     */
    public static function RecordsetToArray($db, $sqlFileName=null, $params=null, $do_not_escape=false)
    {
        $return_val = array();

        if (!empty($sqlFileName)) {
            $sql = SqlHelper::LoadSQL($sqlFileName, $db, $params, $do_not_escape);
            $db->open_recordset($sql);
        }

        $row = $db->fetch_next_object();
        while ($row) {
            $return_val[] = $row;
            $row = $db->fetch_next_object();
        }

        return $return_val;
    }
}
