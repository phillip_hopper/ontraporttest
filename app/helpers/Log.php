<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 12/19/16
 * Time: 3:09 PM
 */

namespace Helpers;

use Sibertec\Helpers\StringHelper;

class Log
{
    private static $eventLogFile;
    private static $errorLogFile;
    private static $settings;

    public static function LogEvent($message)
    {
        if (empty(self::$settings))
            self::GetSettings();

        if (!self::$settings['log_events'])
            return;

        if (empty(self::$eventLogFile))
            self::$eventLogFile = StringHelper::PathCombine(LOG_DIR, 'event_log.txt');

        self::CheckFileSize(self::$eventLogFile);

        $entry = date('Y-m-d H:i:s') . ": $message\n";
        file_put_contents(self::$eventLogFile, $entry, FILE_APPEND);
    }

    public static function LogError($message)
    {
        if (empty(self::$settings))
            self::GetSettings();

        if (!self::$settings['log_errors'])
            return;

        if (empty(self::$errorLogFile))
            self::$errorLogFile = StringHelper::PathCombine(LOG_DIR, 'error_log.txt');

        self::CheckFileSize(self::$errorLogFile);

        $entry = date('Y-m-d H:i:s') . ": $message\n";
        file_put_contents(self::$errorLogFile, $entry, FILE_APPEND);
    }

    private static function GetSettings()
    {
        $file_name = StringHelper::PathCombine(CONFIG_DIR, 'log_settings.json');
        self::$settings = json_decode(file_get_contents($file_name), true);
    }

    private static function CheckFileSize($file_name)
    {
        if (!is_file($file_name)) {
            file_put_contents($file_name, '');
            chmod($file_name, 0666);
        }

        $bytes = filesize($file_name);

        // OK if file doesn't exist
        if (empty($bytes))
            return;

        // OK if under 5MB
        if ($bytes < (1024 * 1024 * 5))
            return;

        $suffix = date('YmdHis');

        rename($file_name, $file_name . '.' . $suffix);
    }
}
