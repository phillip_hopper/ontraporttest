<?php


namespace Helpers;


class OntraportCondition
{
    public $field;
    public $op;
    public $value;

    public function __construct($field_name, $operation, $value)
    {
        $this->field = ['field' => $field_name];
        $this->op    = $operation;
        $this->value = ['value' => $value];
    }
}
