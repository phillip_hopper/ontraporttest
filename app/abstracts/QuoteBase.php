<?php

declare(strict_types=1);

namespace Abstracts;


use Controllers\AppData;
use Controllers\DataClassEx;
use Exception;
use Interfaces\IQuote;
use Models\OntraportContact;
use Sibertec\Helpers\DateHelper;
use Sibertec\Helpers\Request;
use Sibertec\Helpers\StringHelper;

abstract class QuoteBase extends DataClassEx implements IQuote
{
    public $IsDuplicate = false;
    private $cleaned_email;


    public abstract function FromPostData();

    public abstract function SendToOntraport(OntraportContact $contact);

    /**
     * @throws Exception
     */
    public function GetCommonPostData()
    {
        // either phone or email is required
        $phone = Request::PostStr('phone');
        $email = Request::PostStr('email');
        if (empty($phone) && empty($email))
            throw new Exception('Must have either phone or email.');

        // version must ge 4 or greater
        $version = Request::PostInt('ver');
        if (empty($version) || $version < 4)
            throw new Exception('Version must be 4 or greater.');

        $full_name = Request::PostStr('fullName');

        // make sure source doesn't start with http
        $source = strtolower(Request::PostStr('source'));
        $source = preg_replace('/^https?:\/\//', '', $source);

        // make sure source doesn't start with www
        $source = preg_replace('/^www\./', '', $source);

        // make sure $page doesn't start with $source
        $page = Request::PostStr('page');
        $pos  = strpos($page, $source);
        if ($pos !== false && $pos < 10)
            $page = substr($page, $pos + strlen($source));

        // make sure page doesn't start with /
        if ($page == '/')
            $page = '';
        elseif (StringHelper::BeginsWith($page, '/'))
            $page = substr($page, 1);

        $this->full_name  = $full_name;
        $this->state      = Request::PostStr('state');
        $this->email      = $email;
        $this->phone      = $phone;
        $this->sms        = Request::PostBool('sms');
        $this->offer      = Request::PostStr('offer', null);
        $this->mobile     = Request::PostBool('mobile');
        $this->source     = $source;
        $this->page       = $page;
        $this->utm        = Request::PostStr('utm', null);
        $this->ip_address = Request::PostStr('remoteAddress');
        $this->user_agent = Request::PostStr('userAgent');
        $this->version    = $version;
        $this->visitor_id = self::GetVisitorId();
    }


    /**
     * @return string
     * @throws Exception
     */
    private static function GetVisitorId()
    {
        $me_id = Request::PostStr('me');

        if (empty($me_id))
            return '0';

        $sql = <<<SQL
SELECT id
FROM analytics.visitor
WHERE unique_id = '{$me_id}';
SQL;
        $big_id = AppData::MainDatabase()->execute_scalar_string($sql);

        return $big_id ?: '0';
    }

    /**
     * @param $note
     *
     * @throws Exception
     */
    public function AddNote($note)
    {
        $stamped = DateHelper::ToShortDateTimeString('now') . ': ' . $note;

        if (empty($this->notes))
            $this->notes = $stamped;
        else
            $this->notes .= "\n\n" . $stamped;

        $this->SaveNow(AppData::MainDatabase());
    }


    /**
     * @throws Exception
     */
    public function MarkSent()
    {
        if (empty($this->sent_at))
            $this->sent_at = DateHelper::DateTimeStrToSQL('now');

        $this->SaveNow(AppData::MainDatabase());
    }

    /**
     * @throws Exception
     */
    public function MarkProcessed()
    {
        if (empty($this->processed_at))
            $this->processed_at = DateHelper::DateTimeStrToSQL('now');

        $this->SaveNow(AppData::MainDatabase());
    }

    /**
     * @throws Exception
     */
    public function MarkRejected()
    {
        if (empty($this->rejected_at))
            $this->rejected_at = DateHelper::DateTimeStrToSQL('now');

        $this->SaveNow(AppData::MainDatabase());
    }

    public function CleanedEmail()
    {
        if (!is_null($this->cleaned_email))
            return $this->cleaned_email;

        // clean and validate email
        if (!empty($this->email)) {

            // fix the double-dot error
            $this->cleaned_email = preg_replace('/[.]{2,}/', '.', $this->email);

            if (!(filter_var($this->cleaned_email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $this->cleaned_email)))
                $this->cleaned_email = '';
        }
        else {
            $this->cleaned_email = '';
        }

        return $this->cleaned_email;
    }
}
