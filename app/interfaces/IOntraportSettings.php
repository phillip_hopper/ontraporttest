<?php


namespace Interfaces;


/**
 * Interface IOntraportSettings
 *
 * @property string api_app_id
 * @property string api_key
 * @property int pause_milliseconds
 * @property bool debug
 *
 * @package Interfaces
 */
interface IOntraportSettings
{

}
