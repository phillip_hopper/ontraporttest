<?php


namespace Interfaces;


/**
 * Interface IXUnit
 *
 * @property string color
 * @property string options
 * @property int price
 *
 * @package Interfaces
 */
interface IXUnit extends IModel
{
}
