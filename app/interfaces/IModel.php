<?php


namespace Interfaces;


/**
 * Interface IModel
 *
 * @property int model_id
 * @property int make_id
 * @property string model_name
 * @property string make_name
 * @property string manufacturer
 * @property string category
 * @property float map
 * @property float base_price
 * @property float model_discounts
 * @property float quote_discount
 * @property int quote_template_id
 *
 * @package Interfaces
 */
interface IModel
{
}
