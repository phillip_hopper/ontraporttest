<?php


namespace Interfaces;


/**
 * Interface IQuote
 *
 * @property int id
 * @property int visitor_id
 * @property string full_name
 * @property string state
 * @property string email
 * @property string phone
 * @property bool sms
 * @property float price
 * @property float discount
 * @property int quote_template_id
 * @property int ontraport_contact_id
 * @property int ontraport_quote_id
 * @property string offer
 * @property bool mobile
 * @property string source
 * @property string page
 * @property string utm
 * @property string ip_address
 * @property string user_agent
 * @property int version
 * @property string received_at
 * @property string processed_at
 * @property string sent_at
 * @property string rejected_at
 * @property string notes
 *
 * @package Interfaces
 */
interface IQuote
{

}
