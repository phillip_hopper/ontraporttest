<?php


namespace Interfaces;


/**
 * Interface IQuoteDesign
 *
 * @property bool x_unit
 * @property int model_id
 * @property string make_name
 * @property string model_name
 * @property string option_ids
 * @property string options
 * @property string color
 *
 * @package Interfaces
 */
interface IQuoteDesign extends IQuote
{
}
