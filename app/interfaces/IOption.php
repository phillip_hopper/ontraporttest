<?php


namespace Interfaces;


/**
 * Interface IOption
 *
 * @property int id
 * @property string full_name
 * @property string option_description
 * @property float price
 * @property float discount
 * @property int qty
 *
 * @package Interfaces
 */
interface IOption
{
}
