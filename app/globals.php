<?php

use Sibertec\Helpers\StringHelper;

define('DS', DIRECTORY_SEPARATOR);
define('DEBUG', true);

define('PROJECT_DIR', dirname(__DIR__));
define('APP_DIR', StringHelper::RealPathCombine(PROJECT_DIR, 'app'));
define('CONFIG_DIR', StringHelper::RealPathCombine(PROJECT_DIR, 'config'));
define('LOG_DIR', StringHelper::RealPathCombine(PROJECT_DIR, 'logs'));
define('SQL_DIR', StringHelper::RealPathCombine(APP_DIR, 'sql'));
