SELECT k.label AS make_name, k.id AS make_id, d.full_name AS model_name, d.id AS model_id,
  f.label AS manufacturer, d.map,
  design_2018.uf_rv_category(d.is_toy_hauler, k.hitch_id, k.rv_type_id) AS category,
  design_2018.uf_model_base_price(d.id) AS base_price,
  design_2018.uf_model_discounts(d.id) AS model_discounts,
  inventory_2019.uf_get_unit_discount(NULL, FALSE, TRUE, k.finance_discount, k.stock_discount, k.on_order_discount, k.ordered_discount, FALSE) AS quote_discount,
  IFNULL(k.quote_template_id, 1) AS quote_template_id
FROM design_2018.make AS k INNER JOIN design_2018.model AS d ON k.id = d.make_id
  INNER JOIN design_2018.manufacturer AS f ON k.manufacturer_id = f.id
WHERE d.id = '@model_id';
