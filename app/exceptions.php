<?php

use Helpers\Log;

/* set this to prevent the time zone warning */
date_default_timezone_set('America/New_York');


/**
 * @param $err_level
 * @param $err_message
 * @param string $err_file
 * @param int $err_line
 * @param null $err_context
 *
 * @throws ErrorException
 */
function error_handler($err_level, $err_message, $err_file = '', $err_line = -1, /** @noinspection PhpUnusedParameterInspection */
    $err_context = null)
{
    throw new ErrorException($err_message, 0, $err_level, $err_file, $err_line);
}

/**
 *
 * @param Exception $exception
 * @param bool $just_log
 */
function exception_handler($exception, $just_log=true)
{
    if (!$just_log)
        echo $exception->getMessage() . PHP_EOL;

    Log::LogError("File: {$exception->getFile()}, Line: {$exception->getLine()}, Error: [{$exception->getCode()}] {$exception->getMessage()}");

}

function enable_error_handling()
{
    set_error_handler('error_handler', E_ALL & ~ (E_NOTICE | E_USER_NOTICE | E_WARNING | E_USER_WARNING));
    set_exception_handler('exception_handler');
}

/** @noinspection PhpUnused */
function disable_error_handling()
{
    restore_error_handler();
    restore_exception_handler();
}

enable_error_handling();
