<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 7/2/18
 * Time: 3:39 PM
 */

namespace Models;

use Controllers\AppData;
use Database;
use DataCollection;
use Exception;
use Helpers\SqlHelper;

if (!defined('AUTHORIZED')) die();

class Standards extends DataCollection
{
    private static $total_price = -1;

    /**
     * Standards constructor.
     * @param Database $db
     */
    function __construct($db)
    {
        parent::__construct('Models\Standard', $db);
    }

    /**
     * @return Standards
     * @throws Exception
     */
    private static function GetAll()
    {
        $db = AppData::MainDatabase();
        $sql = SQLHelper::LoadSQL('get_standards.sql', $db);

        /* @var $col Standards */
        $col = new Standards($db);
        $col->sql = $sql;
        $col->load_collection();
        return $col;
    }

    /**
     * @return float
     * @throws Exception
     */
    public static function GetTotalPrice()
    {
        if (self::$total_price != -1)
            return self::$total_price;

        self::$total_price = 0;

        foreach(self::GetAll()->GetCollectionAsArray() as $stn) {
            self::$total_price += ($stn->price - $stn->discount);
        }

        return self::$total_price;
    }
}
