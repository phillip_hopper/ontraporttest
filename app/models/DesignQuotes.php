<?php

declare(strict_types=1);

namespace Models;


use Controllers\AppData;
use DataClass;
use DataCollection;
use Exception;
use Helpers\SqlHelper;

if (!defined('AUTHORIZED'))
    die();

class DesignQuotes extends DataCollection
{
    public function __construct($db)
    {
        parent::__construct('Models\DesignQuote', $db);
    }

    /**
     * Gets list of quotes that need sent
     *
     * @return DesignQuote[]
     * @throws Exception
     */
    public static function GetQuotesToSend()
    {
        $db = AppData::MainDatabase();
        $sql = SqlHelper::LoadSQL('get_design_quotes_to_send.sql', $db);

        /* @var $col DesignQuotes */
        $col = new DesignQuotes($db);
        $col->sql = $sql;
        $col->load_collection();

        return $col->GetCollectionAsArray();
    }

    /**
     * Gets a quote
     *
     * @param int $id
     *
     * @return DesignQuote|DataClass
     * @throws Exception
     */
    public static function GetQuoteById(int $id)
    {
        $db = AppData::MainDatabase();
        $sql = 'SELECT * FROM quotes_2019.quote_design WHERE id = ' . (string)$id;

        /* @var $col DesignQuotes */
        $col = new DesignQuotes($db);
        $col->sql = $sql;

        return $col->get_one_object();
    }
}
