<?php

declare(strict_types=1);

namespace Models;

if (!defined('AUTHORIZED')) die();

class OntraportDesignUnit
{
    public $ID;
    public $Model;
    public $Color;
    public $Price;

    /**
     * OntraportDesignUnit constructor.
     * @param array $unit_data
     */
    public function __construct($unit_data)
    {
        $this->ID    = $unit_data['id'];
        $this->Model = $unit_data[OntraportDesignUnitFields::$Model];
        $this->Color = $unit_data[OntraportDesignUnitFields::$Color];
        $this->Price = $unit_data[OntraportDesignUnitFields::$Price];
    }
}
