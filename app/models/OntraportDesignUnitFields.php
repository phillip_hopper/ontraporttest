<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 11/16/16
 * Time: 9:15 AM
 */

namespace Models;

if (!defined('AUTHORIZED')) die();

/**
 * This is an enum for the Ontraport Design fields
 */
class OntraportDesignUnitFields {
    public static $ObjectTypeID = 10000;
    public static $Year = 'f1667';
    public static $Make = 'f1668';
    public static $Model = 'f1669';
    public static $StockNum = 'f1670';
    public static $Category = 'f1688';
    public static $Color = 'f1689';
    public static $Price = 'f1690';
    public static $Options = 'f1691';
}
