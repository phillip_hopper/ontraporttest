<?php

declare(strict_types=1);

namespace Models;


use Controllers\AppData;
use Database;
use DataCollection;
use Exception;
use Helpers\SqlHelper;

if (!defined('AUTHORIZED'))
    die();

class XUnits extends DataCollection
{
    /** @var XUnit[] */
    private static $loaded_xunits = [];

    /**
     * XUnits constructor.
     * @param Database $db
     */
    function __construct($db)
    {
        parent::__construct('Models\XUnit', $db);
    }

    /**
     * @param int $model_id
     *
     * @return XUnit
     * @throws Exception
     */
    public static function GetByID($model_id)
    {
        if (array_key_exists($model_id, self::$loaded_xunits))
            return self::$loaded_xunits[$model_id];

        $db = AppData::MainDatabase();
        $sql = SQLHelper::LoadSQL('get_x_unit_by_model_id.sql', $db, ['@model_id' => $model_id]);

        /* @var $col XUnits */
        $col = new XUnits($db);
        $col->sql = $sql;
        self::$loaded_xunits[$model_id] = $col->get_one_object();

        return self::$loaded_xunits[$model_id];
    }
}
