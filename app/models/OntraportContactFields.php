<?php

declare(strict_types=1);

namespace Models;

if (!defined('AUTHORIZED')) die();

/**
 * This is an enum for the Ontraport Contact fields
 */
class OntraportContactFields
{
    public static $Owner = 'owner';
    public static $FirstName = 'firstname';
    public static $LastName = 'lastname';
    public static $State = 'state';
    public static $Email = 'email';
    public static $Email2 = 'f1656';
    public static $SmsPhone = 'sms_number';
    public static $OfficePhone = 'office_phone';

    public static $ReferringPage = 'referral_page';

    public static $UtmSourceTypeID   = 76;
    public static $FirstUtmSource    = 'n_lead_source';
    public static $LastUtmSource     = 'l_lead_source';

    public static $UtmCampaignTypeID = 75;
    public static $FirstUtmCampaign  = 'n_campaign';
    public static $LastUtmCampaign   = 'l_campaign';

    public static $UtmContentTypeID  = 78;
    public static $FirstUtmContent   = 'n_content';
    public static $LastUtmContent    = 'l_content';

    public static $UtmMediumTypeID   = 77;
    public static $FirstUtmMedium    = 'n_medium';
    public static $LastUtmMedium     = 'l_medium';

    public static $UtmTermTypeID     = 79;
    public static $FirstUtmTerm      = 'n_term';
    public static $LastUtmTerm       = 'l_term';
}
