<?php

declare(strict_types=1);

namespace Models;


use Abstracts\QuoteBase;
use Controllers\AppData;
use Exception;
use Helpers\OntraportHelper;
use Helpers\SqlHelper;
use Interfaces\IQuoteDesign;
use Sibertec\Helpers\Request;

if (!defined('AUTHORIZED'))
    die();

class DesignQuote extends QuoteBase implements IQuoteDesign
{
    /** @var Model|XUnit */
    private $model;

    /**
     * DesignQuote constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct('quote_design');
    }

    public function __destruct()
    {
        if (!empty($this->model))
            unset($this->model);

        parent::__destruct();
    }

    /**
     * Get the values for the design quote from the $_POST data received
     * @throws Exception
     */
    public function FromPostData()
    {
        $model_id = Request::PostInt('modelID');
        if (empty($model_id))
            throw new Exception('Model identifier not found.');

        $this->GetCommonPostData();

        $option_ids = Request::PostStr('optionIDs');
        $color = Request::PostStr('color');

        // remove duplicate requests
        if (self::RequestIsDuplicate($model_id, $option_ids, $this->full_name, $this->phone, $this->CleanedEmail())) {
            $this->IsDuplicate = true;
            return;
        }

        $this->model_id   = $model_id;
        $this->option_ids = $option_ids;
        $this->color      = $color;

        try {
            $this->GetInfoFromModelSetup();
        }
        catch (Exception $e) {
            exception_handler($e, true);
        }

        $this->SaveNow(AppData::MainDatabase());
    }

    /**
     * @throws Exception
     */
    private function GetInfoFromModelSetup()
    {
        $model = $this->GetModel();
        $model->SetOptionIDs($this->option_ids);

        $this->options           = json_encode($model->GetOptionDescriptions());
        $this->price             = $model->GetTotalPrice();
        $this->discount          = (int)$model->quote_discount;
        $this->make_name         = $model->make_name;
        $this->model_name        = $model->model_name;
        $this->quote_template_id = $model->quote_template_id;
    }

    /**
     * @throws Exception
     */
    public function FromXPostData()
    {
        $model_id = Request::PostInt('xID');
        if (empty($model_id))
            throw new Exception('X-Unit identifier not found.');

        $this->GetCommonPostData();

        $this->model_id = $model_id;
        $unit = self::GetXUnit();

        // remove duplicate requests
        if (self::RequestIsXUnitDuplicate($model_id, $unit->options, $this->full_name, $this->phone, $this->CleanedEmail())) {
            $this->IsDuplicate = true;
            unset($unit);
            return;
        }

        $this->x_unit            = true;
        $this->options           = json_encode(explode("\n", $unit->options));
        $this->color             = $unit->color;
        $this->price             = (int)$unit->price;
        $this->discount          = (int)$unit->quote_discount;
        $this->make_name         = $unit->make_name;
        $this->model_name        = $unit->model_name;
        $this->quote_template_id = $unit->quote_template_id;

        $this->SaveNow(AppData::MainDatabase());

        unset($unit);
    }

    /**
     * @param $model_id
     * @param $option_ids
     * @param $full_name
     * @param $phone
     * @param $email
     *
     * @return mixed|null
     * @throws Exception
     */
    private static function RequestIsDuplicate($model_id, $option_ids, $full_name, $phone, $email)
    {
        $db = AppData::MainDatabase();
        $params = [
            '@model_id' => $model_id,
            '@option_ids' => $option_ids,
            '@name' => $full_name,
            '@phone' => $phone,
            '@email' => $email
        ];
        $sql = SqlHelper::LoadSQL('search_for_design_dupes.sql', $db, $params);
        return $db->execute_scalar_bool($sql);
    }

    /**
     * @param $model_id
     * @param $options
     * @param $full_name
     * @param $phone
     * @param $email
     *
     * @return mixed|null
     * @throws Exception
     */
    private static function RequestIsXUnitDuplicate($model_id, $options, $full_name, $phone, $email)
    {
        $db = AppData::MainDatabase();
        $params = [
            '@model_id' => $model_id,
            '@options' => $options,
            '@name' => $full_name,
            '@phone' => $phone,
            '@email' => $email
        ];
        $sql = SqlHelper::LoadSQL('search_for_x_unit_dupes.sql', $db, $params);
        return $db->execute_scalar_bool($sql);
    }

    /**
     * @return Model
     * @throws Exception
     */
    public function GetModel()
    {
        if (!empty($this->model))
            return $this->model;

        $this->model = Models::GetByID($this->model_id);

        if (empty($this->model))
            throw new Exception('Requested model not found.');

        return $this->model;
    }

    /**
     * @return XUnit
     * @throws Exception
     */
    public function GetXUnit()
    {
        if (!empty($this->model))
            return $this->model;

        $this->model = XUnits::GetByID($this->model_id);

        if (empty($this->model))
            throw new Exception('Requested X-Unit not found.');

        return $this->model;
    }

    /**
     * @param OntraportContact $contact
     *
     * @throws Exception
     */
    public function SendToOntraport(OntraportContact $contact)
    {
        $this->ontraport_contact_id = $contact->ID;

        try {
            $model = $this->GetModel();

            $options = json_decode((string)$this->options, true);

            // add the color
            array_unshift($options, 'Color: ' . $this->color);

            // if this is an x unit, add the stock number
            if (!empty($this->x_unit))
                array_unshift($options, 'STOCK UNIT #: X' . str_pad($this->model_id, 6, '0', STR_PAD_LEFT));

            // format the option descriptions
            $descriptions = implode("\n" . str_repeat('=', 64) . "\n", $options);

            $op_unit = OntraportHelper::AddDesignUnit(
                $this->make_name . ' ' . $this->model_name,
                $model->category,
                $this->color,
                $this->price,
                $descriptions
            );

            $op_quote = OntraportHelper::AddDesignQuote(
                $contact->ID,
                $op_unit->ID,
                $this->price,
                $this->discount,
                $this->source,
                $descriptions,
                $contact->OwnerID,
                $this->phone,
                $this->CleanedEmail(),
                $this->sms,
                $this->offer
            );

            if (!empty($op_quote->ID))
                $this->ontraport_quote_id = $op_quote->ID;

            OntraportHelper::AddTagByName($contact->ID, [$model->category, $this->make_name]);

            $this->MarkProcessed();

            unset($model);
            unset($options);
            unset($descriptions);
            unset($op_unit);
            unset($op_quote);
        }
        catch (Exception $e) {
            exception_handler($e, true);
        }
    }

    /**
     * @throws Exception
     */
    public function ManualRecalc()
    {
        $this->GetInfoFromModelSetup();
        $this->SaveNow(AppData::MainDatabase());
    }
}
