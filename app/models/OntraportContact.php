<?php

declare(strict_types=1);

namespace Models;

use Sibertec\Helpers\StringHelper;

if (!defined('AUTHORIZED')) die();

class OntraportContact
{
    public $ID;
    public $Email;
    public $Email2;
    public $FirstName;
    public $LastName;
    public $SmsPhone;
    public $OfficePhone;
    public $OwnerID;
    public $ReferringPage;

    public $FirstSource;
    public $FirstCampaign;
    public $FirstContent;
    public $FirstMedium;
    public $FirstTerm;

    public $LastSource;
    public $LastCampaign;
    public $LastContent;
    public $LastMedium;
    public $LastTerm;

    /**
     * OntraportContact constructor.
     * @param array $contact_data
     */
    public function __construct($contact_data)
    {
        $this->ID = $contact_data['id'];
        $this->Email = $contact_data[OntraportContactFields::$Email];
        $this->Email2 = $contact_data[OntraportContactFields::$Email2] ?? '';
        $this->FirstName = $contact_data[OntraportContactFields::$FirstName];
        $this->LastName = $contact_data[OntraportContactFields::$LastName];
        $this->SmsPhone = $contact_data[OntraportContactFields::$SmsPhone];
        $this->OfficePhone = $contact_data[OntraportContactFields::$OfficePhone];
        $this->OwnerID = $contact_data[OntraportContactFields::$Owner];
        $this->ReferringPage = $contact_data[OntraportContactFields::$ReferringPage] ?? '';

        $this->FirstSource = $contact_data[OntraportContactFields::$FirstUtmSource] ?? '';
        $this->FirstCampaign = $contact_data[OntraportContactFields::$FirstUtmCampaign] ?? '';
        $this->FirstContent = $contact_data[OntraportContactFields::$FirstUtmContent] ?? '';
        $this->FirstMedium = $contact_data[OntraportContactFields::$FirstUtmMedium] ?? '';
        $this->FirstTerm = $contact_data[OntraportContactFields::$FirstUtmTerm] ?? '';

        $this->LastSource = $contact_data[OntraportContactFields::$LastUtmSource] ?? '';
        $this->LastCampaign = $contact_data[OntraportContactFields::$LastUtmCampaign] ?? '';
        $this->LastContent = $contact_data[OntraportContactFields::$LastUtmContent] ?? '';
        $this->LastMedium = $contact_data[OntraportContactFields::$LastUtmMedium] ?? '';
        $this->LastTerm = $contact_data[OntraportContactFields::$LastUtmTerm] ?? '';
    }

    public function HasEmail($email_to_test)
    {
        if (empty($email_to_test))
            return true;

        if ($this->Email == $email_to_test)
            return true;

        return false;
    }

    public function HasPhone($phone_to_test)
    {
        $phone_to_test = StringHelper::FormatPhone($phone_to_test);
        if (empty($phone_to_test))
            return true;

        $office = StringHelper::FormatPhone($this->OfficePhone);
        $sms = StringHelper::FormatPhone($this->SmsPhone);

        if (!empty($office) && ($office == $phone_to_test))
            return true;

        if (!empty($sms) && ($sms == $phone_to_test))
            return true;

        return false;
    }

    public function HasFirstUTM()
    {
        return !empty($this->FirstSource) || !empty($this->FirstCampaign);
    }
}
