<?php

declare(strict_types=1);

namespace Models;

if (!defined('AUTHORIZED')) die();

class OntraportQuote
{
    public $ID;
    public $ContactID;
    public $UnitID;
    public $Price;
    public $FinancingDiscount;
    public $Source;

    /**
     * OntraportQuote constructor.
     * @param array $quote_data
     */
    public function __construct($quote_data)
    {
        $this->ID = $quote_data['id'];
        $this->ContactID = $quote_data[OntraportQuoteFields::$ContactID];
        $this->UnitID = $quote_data[OntraportQuoteFields::$UnitID] ?? 0;
        $this->Price = $quote_data[OntraportQuoteFields::$Price];
        $this->FinancingDiscount = $quote_data[OntraportQuoteFields::$Discount];
        $this->Source = $quote_data[OntraportQuoteFields::$Source];
    }
}
