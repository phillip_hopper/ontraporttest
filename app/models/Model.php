<?php

declare(strict_types=1);

namespace Models;


use Controllers\AppData;
use DataClass;
use Exception;
use Helpers\DataHelper;
use Interfaces\IModel;
use Interfaces\IOption;
use Sibertec\Helpers\StringHelper;

if (!defined('AUTHORIZED'))
    die();

class Model extends DataClass implements IModel
{
    /** @var IOption[] */
    private $factory_options;

    /** @var IOption[] */
    private $dealer_options;

    /** @var string[] */
    private $option_ids;

    /** @var string[] */
    private $option_descriptions;

    /** @var float */
    private $option_price;

    /** @var int */
    private $total_price;

    function __construct()
    {
        parent::__construct();
    }

    function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Sets the factory and dealer option IDs.
     * Either an array, or a string like 'fo_595,fo_601,fo_604,fo_607,fo_611,fo_612,fo_614,fo_616,do_2-3,do_0-1'.
     *
     * @param string|string[]$option_ids
     */
    public function SetOptionIDs($option_ids)
    {
        if (is_array($option_ids))
            $this->option_ids = $option_ids;
        else
            $this->option_ids = explode(',', $option_ids);
    }

    /**
     * @return array|IOption[]
     * @throws Exception
     */
    private function GetFactoryOptions()
    {
        if (!empty($this->factory_options))
            return $this->factory_options;

        if (empty($this->option_ids))
            return [];

        $factory_option_ids  = array_filter($this->option_ids, function($v) { return substr($v, 0, 3) == 'fo_'; });

        if (!empty($factory_option_ids)) {
            $s = implode('\',\'', $factory_option_ids);
            $s = str_replace('fo_', '', $s);

            $this->factory_options = DataHelper::RecordsetToArray(AppData::MainDatabase(), 'get_factory_options.sql', ['@model_id' => $this->model_id, '@option_ids' => $s], true);
        }

        return $this->factory_options;
    }

    /**
     * @return array|IOption[]
     * @throws Exception
     */
    private function GetDealerOptions()
    {
        if (!empty($this->dealer_options))
            return $this->dealer_options;

        if (empty($this->option_ids))
            return [];

        $dealer_option_ids  = array_filter($this->option_ids, function($v) { return substr($v, 0, 3) == 'do_'; });

        if (!empty($dealer_option_ids)) {

            $just_ids = array();
            $qty = array();

            foreach ($dealer_option_ids as $d) {

                $parts = explode('-', $d);
                $id = (int)str_replace('do_', '', $parts[0]);

                $just_ids[] = $id;
                $qty[$id] = (count($parts) > 1 ? (int)$parts[1] : 1);
            }

            $this->dealer_options = DataHelper::RecordsetToArray(AppData::MainDatabase(), 'get_dealer_options.sql', ['@option_ids' => implode('\',\'', $just_ids)], true);

            foreach ($this->dealer_options as &$opt) {
                $opt->qty = $qty[$opt->id];
            }
        }

        return $this->dealer_options;
    }

    /**
     * @throws Exception
     */
    private function GetOptionDescriptionsAndPrice()
    {
        $descriptions = [];
        $price = 0;

        /** @var IOption[] $opts */
        $opts = $this->GetFactoryOptions();

        foreach ($opts as $opt) {

            if (empty($opt->option_description))
                $descriptions[] = $opt->full_name;
            else
                $descriptions[] = $opt->full_name . ': ' . $opt->option_description;

            $price += $opt->price - $opt->discount;
        }

        $opts = $this->GetDealerOptions();

        foreach ($opts as $opt) {

            $qty = $opt->qty;

            if ($qty == 1)
                $descriptions[] = $opt->full_name;
            else
                $descriptions[] = (string)$qty . ' - ' . StringHelper::Pluralize($qty, $opt->full_name);

            $price += $qty * ($opt->price - $opt->discount);
        }

        $this->option_descriptions = $descriptions;
        $this->option_price = $price;
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function GetOptionDescriptions()
    {
        if (is_null($this->option_descriptions))
            $this->GetOptionDescriptionsAndPrice();

        return $this->option_descriptions;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function GetTotalPrice()
    {
        if (!empty($this->total_price))
            return $this->total_price;

        if (is_null($this->option_price))
            $this->GetOptionDescriptionsAndPrice();

        $standards_total = Standards::GetTotalPrice();

        $this->total_price = intval(ceil($this->base_price + $this->option_price + $standards_total - $this->model_discounts));

        return $this->total_price;
    }
}
