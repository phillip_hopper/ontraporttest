<?php

declare(strict_types=1);

namespace Models;

if (!defined('AUTHORIZED')) die();

/**
 * This is an enum for the Ontraport Quote fields
 */
class OntraportQuoteFields {
    public static $ObjectTypeID = 10001;
    public static $UnitID = 'f1686';
    public static $ContactID = 'f1687';
    public static $Owner = 'owner';
    public static $Phone = 'f1681';
    public static $Email = 'f1682';
    public static $Price = 'f1683';
    public static $Discount = 'f1684';
    public static $Source = 'f1692';
    public static $DesignOptions = 'f1693';
    public static $SendSms = 'f1694';
    public static $Offer = 'f1695';
}
