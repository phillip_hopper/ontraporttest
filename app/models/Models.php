<?php

declare(strict_types=1);

namespace Models;


use Controllers\AppData;
use Database;
use DataClass;
use DataCollection;
use Exception;
use Helpers\SqlHelper;

if (!defined('AUTHORIZED'))
    die();

class Models extends DataCollection
{
    /**
     * Models constructor.
     * @param Database $db
     */
    function __construct($db)
    {
        parent::__construct('Models\Model', $db);
    }

    /**
     * @param int $model_id
     *
     * @return Model|DataClass
     * @throws Exception
     */
    public static function GetByID($model_id)
    {
        $db = AppData::MainDatabase();
        $sql = SQLHelper::LoadSQL('get_details_by_model_id.sql', $db, ['@model_id' => $model_id]);

        /* @var $col Models */
        $col = new Models($db);
        $col->sql = $sql;
        return $col->get_one_object();
    }
}
