<?php

declare(strict_types=1);

namespace Models;


use DataClass;
use Interfaces\IXUnit;

if (!defined('AUTHORIZED'))
    die();

class XUnit extends DataClass implements IXUnit
{
    function __construct()
    {
        parent::__construct();
    }

    function __destruct()
    {
        parent::__destruct();
    }
}
