<?php /** @noinspection DuplicatedCode */

declare(strict_types=1);

use Abstracts\QuoteBase;
use Helpers\Blacklist;
use Helpers\OntraportHelper;
use Models\DesignQuotes;
use Sibertec\Helpers\MiscFunctions;
use Sibertec\Helpers\StringHelper;

define('AUTHORIZED', 'yes');

include_once __DIR__ . '/app/app_start.php';

/**
 * @param QuoteBase $quote
 *
 * @return bool
 * @throws Exception
 */
function quote_is_blacklisted(QuoteBase $quote)
{
    if (Blacklist::EmailIsBlacklisted($quote->email)) {
        $quote->AddNote('Not sending to CRM because email is blacklisted.');
        $quote->MarkRejected();
        return true;
    }

    if (Blacklist::SmsIsBlacklisted($quote->phone)) {
        $quote->AddNote('Not sending to CRM because phone number is blacklisted.');
        $quote->MarkRejected();
        return true;
    }

    return false;
}

function parse_utm($raw)
{
    if (empty($raw))
        return [];

    $pieces = explode('&', $raw);
    $return_val = array();

    foreach($pieces as $piece) {
        $parts = explode('=', $piece, 2);
        if (count($parts) == 2) {
            $return_val[$parts[0]] = $parts[1];
        }
    }

    // check for the facebook problem
    if (!empty($return_val['src']) && ($return_val['src'] == 'referrer')) {
        if (!empty($return_val['con'])) {
            if (StringHelper::Contains($return_val['con'], 'facebook')) {
                $return_val['src'] = 'facebook';
            }
        }
    }

    return $return_val;
}

try {

    $max_id = OntraportHelper::GetMaxContactId();
    $new_id = $max_id;
    $counter = 0;

    $design_quotes = DesignQuotes::GetQuotesToProcess();

    foreach($design_quotes as $quote) {

        // give each quote a new 3 minute timeout
        set_time_limit(180);

        // check the blacklist
        if (quote_is_blacklisted($quote))
            continue;

        $utm = parse_utm($quote->utm);

        $contact = OntraportHelper::GetOrCreateContact(
            $quote->full_name,
            $quote->source,
            $quote->state,
            $quote->phone,
            $quote->CleanedEmail(),
            $quote->sms,
            true,
            false,
            ($utm['src'] ?? ''),
            ($utm['med'] ?? ''),
            ($utm['cam'] ?? ''),
            ($utm['ter'] ?? ''),
            ($utm['con'] ?? '')
        );

        OntraportHelper::UpdateContact($contact, $quote->phone, $quote->CleanedEmail(), $quote->sms);

        $quote->SendToOntraport($contact);

        if ($contact->ID > $max_id) {

            if ($contact->ID > ($max_id + 1)) {
                MiscFunctions::DebugPrint("ERROR: quote = {$quote->id}, max_id = {$max_id}, new_id = {$new_id}.");
                break;
            }

            $max_id = $contact->ID;
        }

        $memory_used = (int)(memory_get_usage());
        $kbs_used = round($memory_used / 1024, 2);

        MiscFunctions::DebugPrint("Memory Used = {$kbs_used} KB.");
    }

}
catch (Exception $e) {
    exception_handler($e, false);
}

MiscFunctions::DebugPrint("Finished: counter = {$counter}.");
