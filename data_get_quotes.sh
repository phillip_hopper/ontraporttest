#!/usr/bin/env bash

dumpDir="/tmp/rvw-data-2/"

echo "Creating ${dumpDir}"
rm -rf "${dumpDir}"
mkdir -p "${dumpDir}"


declare -a dbs=("quotes_2019"
                )


for db in "${dbs[@]}"
do
   echo "Backing up ${db}"
   mysqldump --defaults-file=config/my.aws.conf --default-character-set=utf8 --add-drop-database --routines --result-file="${dumpDir}${db}.sql" --databases "${db}"

   echo "Cleaning up ${db}"
   sed -i -E 's/\/\*\!50017\sDEFINER=.+`@`(%|localhost)`\*\// /g' "${dumpDir}${db}.sql"
   sed -i -E 's/\sDEFINER=.+`@`(%|localhost)`\s/ /g' "${dumpDir}${db}.sql"

   echo "Restoring to ${db}"
   mysql --defaults-file=config/my.local.conf --default-character-set=utf8 < "${dumpDir}${db}.sql"
done


echo "Finished"
