<?php

declare(strict_types=1);

use Helpers\OntraportHelper;
use Sibertec\Helpers\MiscFunctions;

define('AUTHORIZED', 'yes');

include_once __DIR__ . '/app/app_start.php';

try {

    $max_id = OntraportHelper::GetMaxContactId();
    $new_id = $max_id;
    $counter = 0;

    while ($counter < 2000 && $new_id < ($max_id + 2)) {

        $counter++;
        $id = $max_id + 1;
        $first_name = 'First' . (string)$id;
        $last_name  = 'Last' . (string)$id;
        $email      = 'employee' . (string)$id . '@rvwholesalers.com';
        $f1656      = 'employee' . (string)$id . '_2@rvwholesalers.com';

        // check for a duplicate
        $data = OntraportHelper::GetContact($email);

        if (!empty($data)) {
            MiscFunctions::DebugPrint("Contact {$email} already exists.");
            break;
        }

        // add the contact
        $data = OntraportHelper::AddContact($first_name, $last_name, $email);

        $new_id = (int)$data['id'];

        OntraportHelper::UpdateContact($new_id, $f1656);

        MiscFunctions::DebugPrint("Added contact {$new_id}.");

        if ($new_id !== $max_id + 1) {

            MiscFunctions::DebugPrint("ERROR1: max_id = {$max_id}, new_id = {$new_id}.");
            break;
        }

        $max_id = OntraportHelper::GetMaxContactId();

        if ($new_id !== $max_id) {

            MiscFunctions::DebugPrint("ERROR2: max_id = {$max_id}, new_id = {$new_id}.");
            break;
        }

        $memory_used = (int)(memory_get_usage());
        $megs_used = round($memory_used / 1024 / 1024, 2);

        MiscFunctions::DebugPrint("Memory Used = {$megs_used} MB.");
    }
}
catch (Exception $e) {
    exception_handler($e, false);
}

MiscFunctions::DebugPrint("Finished: counter = {$counter}.");
